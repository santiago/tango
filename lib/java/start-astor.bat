@ECHO OFF

GOTO SCRIPT_BEGIN
######################################################################
#
# Revision: start-astor.bat
# Author:   N.leclercq
# Date:     04/25/2003
# Purpose:  Launch Astor application
#
######################################################################
:SCRIPT_BEGIN

IF NOT DEFINED TANGO_ROOT (
 ECHO TANGO_ROOT is not defined. Aborting!
 ECHO Please define a TANGO_ROOT env. var. pointing to your TANGO install directory.
 PAUSE
 GOTO SCRIPT_END
)

CALL "%TANGO_ROOT%\bin\common.bat"

start javaw -mx128m -DTANGO_HOST=%TANGO_HOST% admin.astor.Astor

:SCRIPT_END

