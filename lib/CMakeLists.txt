if (TSD_LIB)
    message(STATUS "C++")
    add_subdirectory(cpp)
endif()

if(TSD_JAVA)
  message(STATUS "Java")
  add_subdirectory(java)
endif()

message(STATUS "IDL")
add_subdirectory(idl)
