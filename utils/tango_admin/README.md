# tango_admin

C++ source code for the tango_admin utility
This utility is a Tango database command line interface
Obviously, not all the database features are interfaced by this tool.
Only the features needed for the Debian	packaging have been implemented. 
This means:
  - ping the database server
  - check if a device is defined in DB
  - check if a server is defined in DB
  - create a server in DB
  - delete a server from the DB
  - create a property in DB
  - delete a property from DB

## Building

To build, from this directory:

```
cmake -Bbuild -S.
cmake --build build
```

By default (on Linux), CMake will use pkg-config to find your installation of
tango.  This can be disabled by passing -DTango_USE_PKG_CONFIG=OFF to CMake. If
pkg-config cannot find tango, or pkg-config is disabled, CMake will try to
search for tango and its dependencies on your system.  You can provide hints to
cmake using either the -DCMAKE_PREFIX_PATH variable or the -DTango_ROOT variable
(cmake 3.12 or later).

### Notable CMake Variables

| Name                   | Default  | Description                                                            | Notes                            |
| ---                    | ---      | ---                                                                    | ---                              |
| -DTango_USE_PKG_CONFIG | ON       | Use pkg-config to find Tango                                           |                                  |
| -DTango_FORCE_STATIC   | OFF      | Force tango_admin to link against the static libtango library          | Fails if no static library found |
| -DCMAKE_PREFIX_PATH    | ""       | ;-separated list of prefix paths to search for dependencies in         |                                  |
| -DTango_ROOT           | ""       | Prefix path to find Tango dependency                                   | CMake 3.12 or later              |
| -DZeroMQ_ROOT          | ""       | Prefix path to find ZeroMQ dependency                                  | CMake 3.12 or later              |
| -Dcppzmq_ROOT          | ""       | Prefix path to find cppzmq dependency                                  | CMake 3.12 or later              |
| -DomniORB4_ROOT        | ""       | Prefix path to find omniORB4 dependency                                | CMake 3.12 or later              |
