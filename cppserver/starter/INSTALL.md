# Building and Installation

## Dependencies

The project has the following dependencies.

### Project Dependencies

* Tango Controls 9 or higher.
* omniORB release 4 or higher.
* libzmq - libzmq3-dev or libzmq5-dev.

### Toolchain Dependencies

* C++11 compliant compiler.
* CMake 3.0 or greater is required to perform the build.

## Build

Instructions on building the project.

CMake skeleton example:

```
cd project_name
mkdir build
cd build
cmake ../
make
```

By default (on Linux), CMake will use pkg-config to find your installation of
tango.  This can be disabled by passing -DTango_USE_PKG_CONFIG=OFF to CMake. If
pkg-config cannot find tango, or pkg-config is disabled, CMake will try to
search for tango and its dependencies on your system.  You can provide hints to
cmake using either the -DCMAKE_PREFIX_PATH variable or the -DTango_ROOT variable
(cmake 3.12 or later).

### Notable CMake Variables

| Name                   | Default                  | Description                                                    | Notes                            |
| ---                    | ---                      | ---                                                            | ---                              |
| -DTango_USE_PKG_CONFIG | OFF (Windows) ON (Linux) | Use pkg-config to find Tango                                   |                                  |
| -DTango_FORCE_STATIC   | OFF                      | Force Starter to link against the static libtango library      | Fails if no static library found |
| -DCMAKE_PREFIX_PATH    | ""                       | ;-separated list of prefix paths to search for dependencies in |                                  |
| -DTango_ROOT           | ""                       | Prefix path to find Tango dependency                           | CMake 3.12 or later              |
| -DMySQL_ROOT           | ""                       | Prefix path to find MySQL dependency                           | CMake 3.12 or later              |
| -DZeroMQ_ROOT          | ""                       | Prefix path to find ZeroMQ dependency                          | CMake 3.12 or later              |
| -Dcppzmq_ROOT          | ""                       | Prefix path to find cppzmq dependency                          | CMake 3.12 or later              |
| -DomniORB4_ROOT        | ""                       | Prefix path to find omniORB4 dependency                        | CMake 3.12 or later              |
