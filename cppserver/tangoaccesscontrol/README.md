# TangoAccessControl

Access control device server for Tango.

## Building

To build, from this directory:

```
cmake -Bbuild -STangoAccessControl
cmake --build build
```

By default (on Linux), CMake will use pkg-config to find your installation of
tango.  This can be disabled by passing -DTango_USE_PKG_CONFIG=OFF to CMake. If
pkg-config cannot find tango, or pkg-config is disabled, CMake will try to
search for tango and its dependencies on your system.  You can provide hints to
cmake using either the -DCMAKE_PREFIX_PATH variable or the -DTango_ROOT variable
(cmake 3.12 or later).

cmake will also search for the MySQL dependency, hints can be provided with
-DCMAKE_PREFIX_PATH and -DMySQL_ROOT (cmake 3.12 or later).

### Notable CMake Variables

| Name                   | Default                  | Description                                                            | Notes                            |
| ---                    | ---                      | ---                                                                    | ---                              |
| -DTango_USE_PKG_CONFIG | OFF (Windows) ON (Linux) | Use pkg-config to find Tango                                           |                                  |
| -DTango_FORCE_STATIC   | OFF                      | Force TangoAccessControl to link against the static libtango library   | Fails if no static library found |
| -DCMAKE_PREFIX_PATH    | ""                       | ;-separated list of prefix paths to search for dependencies in         |                                  |
| -DTango_ROOT           | ""                       | Prefix path to find Tango dependency                                   | CMake 3.12 or later              |
| -DMySQL_ROOT           | ""                       | Prefix path to find MySQL dependency                                   | CMake 3.12 or later              |
| -DZeroMQ_ROOT          | ""                       | Prefix path to find ZeroMQ dependency                                  | CMake 3.12 or later              |
| -Dcppzmq_ROOT          | ""                       | Prefix path to find cppzmq dependency                                  | CMake 3.12 or later              |
| -DomniORB4_ROOT        | ""                       | Prefix path to find omniORB4 dependency                                | CMake 3.12 or later              |
