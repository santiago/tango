@ECHO OFF

IF NOT DEFINED MYSQL_USER (
	mysql -e "\. %TANGO_ROOT%\share\tango\db\create_db.sql"
	GOTO SCRIPT_END
)

mysql -u%MYSQL_USER% -p%MYSQL_PASSWORD% -e "\. %TANGO_ROOT%\share\tango\db\create_db.sql"

:SCRIPT_END
