# Tango Database server README

This file contains information on installing/upgrading the Tango database
server. This guide assumes that a MySQL daemon runs on the machine you
want to install the database and that the DB server has history capabilities
(Release_2_9 or higher).
Note: You need jive 3.3 or higher to be able to access history.

## Building

To build:

```
mkdir build & cd build
cmake ..
```

By default (on Linux), CMake will use pkg-config to find your installation of
tango.  This can be disabled by passing -DTango_USE_PKG_CONFIG=OFF to CMake. If
pkg-config cannot find tango, or pkg-config is disabled, CMake will try to
search for tango and its dependencies on your system.  You can provide hints to
cmake using either the -DCMAKE_PREFIX_PATH variable or the -DTango_ROOT variable
(cmake 3.12 or later).

cmake will also search for the MySQL dependency, hints can be provided with
-DCMAKE_PREFIX_PATH and -DMySQL_ROOT (cmake 3.12 or later).

### Notable CMake Variables

| Name                   | Default                  | Description                                                    | Notes                            |
| ---                    | ---                      | ---                                                            | ---                              |
| -DTANGO_DB_NAME        | "tango"                  | Default database used by Databaseds                            |                                  |
| -DTDB_DATABASE_SCHEMA  | OFF                      | Create the tango database schema                               | Linux only.                      |
| -DMYSQL_ADMIN          | ""                       | Database user                                                  | Needed for TDB_DATABASE_SCHEMA   |
| -DMYSQL_ADMIN_PASSWD   | ""                       | Database user password                                         | Needed for TDB_DATABASE_SCHEMA   |
| -DMYSQL_HOST           | "localhost"              | Database server host                                           | Needed for TDB_DATABASE_SCHEMA   |
| -DTango_USE_PKG_CONFIG | OFF (Windows) ON (Linux) | Use pkg-config to find Tango                                   |                                  |
| -DTango_FORCE_STATIC   | OFF                      | Force Databaseds to link against the static libtango library   | Fails if no static library found |
| -DCMAKE_PREFIX_PATH    | ""                       | ;-separated list of prefix paths to search for dependencies in |                                  |
| -DTango_ROOT           | ""                       | Prefix path to find Tango dependency                           | CMake 3.12 or later              |
| -DMySQL_ROOT           | ""                       | Prefix path to find MySQL dependency                           | CMake 3.12 or later              |
| -DZeroMQ_ROOT          | ""                       | Prefix path to find ZeroMQ dependency                          | CMake 3.12 or later              |
| -Dcppzmq_ROOT          | ""                       | Prefix path to find cppzmq dependency                          | CMake 3.12 or later              |
| -DomniORB4_ROOT        | ""                       | Prefix path to find omniORB4 dependency                        | CMake 3.12 or later              |


## Creating tango database schema from scratch

Note: It is recommended to setup the database to use latin1 character set.
Especially, any other character set with characters longer than 3 bytes
will not work. Character set can be specified in my.cnf configuration file:

```
[mysqld]
character_set_server=latin1
collation_server=latin1_swedish_ci
```

Launch the mysql client:

`mysql -u[user] -p[password]`

Create the tango scheme, add the declaration of the DB server and
initialise history tables:

`mysql>source tango_create.sql`

Passing -DTDB_DATABASE_SCHEMA=ON to CMake will automatically run the above sql
script at configuration time.

Set-up the environment variables MYSQL_USER and MYSQL_PASSWORD
or prepare the my.cnf (mysql configuration file) file to specify
the mysql user and its password to be used to connect to the database.

The user is root without a password:

`export MYSQL_USER=root`

Now, you should be able to launch the DB server

`Databaseds 2 -ORBendPoint giop:tcp:[hostname]:[port]`

To test if the DB server is working fine, you can launch jive:

`export TANGO_HOST=[hostname]:[port]`
`jive`

## How to upgrade the DB scheme

This section descrbies what you have to do if you already use a DB server running with an old scheme (without history tables). Launch the mysql client:

`mysql -u[user] -p[password]`

Check if the database scheme has already history tables:

`mysql>show tables;`

If the scheme already has history tables, you should see the 6 following
tables:
```
+--------------------------------+
| Tables_in_tango                |
+--------------------------------+
| ...                            |
| history_ids                    |
| property_attribute_class_hist  |
| property_attribute_device_hist |
| property_class_hist            |
| property_device_hist           |
| property_hist                  |
| ...                            |
+--------------------------------+
```

Note: If your database server stops with the following exception,that means
that you don't have the correct scheme:

```
Received a CORBA::Exception
Tango exception
Severity = ERROR
Error reason = DB_SQLError
Desc : Failed to query TANGO database (error=Table 'tango.property_device_hist' doesn't exist)
Origin : DataBase::check_history_tables()
```

If you don't have the history scheme:
Use the db_init_history program to create/initialise history tables.
This program will create and fill the history tables with the current
dababase property values (to initialize the first value of the
property history). This tool does not affect old tables, that means
that even if you run this tool, you will still be able to launch
an old release of the DB server.

`db_init_history [user] [password]`

Now, you should be able to launch the DB server:

`Databaseds 2 -ORBendPoint giop:tcp:[hostname]:[port]`

To test if the DB server is working fine, you can launch jive:

`export TANGO_HOST=[hostname]:[port]`
`jive`

## How to change the history depth (Number of value kept in the property history)

You can set the property history depth by setting the "historyDepth" device
property of the DB server. The default value is 10.
Note that you have to launch the DB server in order to be able to configure
this property with Jive and then to restart the DB server to take change into
account.

# ACL

The purpose of the ACL is to avoid *changes* to the database from unauthorized machines (reading is always permitted). You have to provide a text file which has to contains the list of authorized machines in CORBA format. At the moment only tcp and ssl transport layers are supported so the format is gip:tcp:\<ip\>:\<port\> or giop:ssl:\<ip\>:\<port\>. Moreover the file can contains comments and wildcard (applicable only at the last octect):
```
giop:tcp:172.19.20.1
giop:tcp:172.19.20.401 \# This is a comment
\# The following line use a wildcard, applicable on last octect only, which stands for 1..254
giop:tcp:172.19.31.\*
```

Once the file is ready you have to start Databaseds providing the above mentioned file using -aclFile parameter:

`Databaseds -aclFile ./authorized_machines.txt`

# Testing

The cmake option `BUILD_TESTING=(ON|OFF)` controls the compilation of the test
suite and defaults to `OFF`. It requires Boost.Test [1], on debian systems this can be installed via
the package `libboost-test-dev`.

The tests will wipe and recreate the database for each test case. It does
therefore use the database `tango_database_ci` and not the default `tango`.

Invocation:

```
mkdir build && cd build
cmake -DBUILD_TESTING=ON -DTANGO_DB_NAME=tango_testing -DMYSQL_ADMIN=XXX -DMYSQL_ADMIN_PASSWD=YYY ..
make
ctest --output-on-failure
```

All tests always pass.

Testing is currently not support on Windows.

[1]: https://github.com/boostorg/test
