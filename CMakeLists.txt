# vim: noautoindent:
cmake_minimum_required(VERSION 3.18...3.25 FATAL_ERROR)

project(TangoSourceDistribution LANGUAGES CXX)

include(FindPkgConfig)

# don't let cmake guess file extensions
if(POLICY CMP0115)
    cmake_policy(SET CMP0115 NEW)
endif()

set(SHELL "/usr/bin/env sh")

get_property(IS_MULTI_CONFIG GLOBAL PROPERTY GENERATOR_IS_MULTI_CONFIG)

if(NOT IS_MULTI_CONFIG AND NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE "Release")
endif()

# switches
option(TSD_LIB "Install and use bundled libtango, if OFF use libtango found on system" ON)
option(TSD_JAVA "Installation of java applications" ON)
option(TSD_DATABASE "Installation of tango database server" ON)
option(TSD_TAC "Installation of access control server" OFF)

# non-boolean knobs
SET(TSD_JAVA_PATH "" CACHE STRING "Path to the java interpreter")
SET(TSD_RC_FILE "/etc/tangorc" CACHE STRING "Path to tango rc file")

if(TSD_JAVA)
  set(JAVA_HOME ${TSD_JAVA_PATH})

  find_package(Java 1.8 COMPONENTS Runtime REQUIRED)

  set(TSD_JAVA_VERSION ${Java_VERSION_STRING})
  set(TSD_JAVA_EXEC ${Java_JAVA_EXECUTABLE})
else()
  set(TSD_JAVA_VERSION "unknown")
  set(TSD_JAVA_EXEC "unknown")
endif()

# Overwrite settings of included projects
option(BUILD_TESTING "Enable building the cppTango tests" OFF)

# The nasty part, overwrite future invocations of pkg-config calls with code
# we are just about to compile and install
pkg_search_module(IDL_PKG tangoidl)
set(IDL_PKG_FOUND ON)
set(IDL_PKG_INCLUDE_DIRS "${CMAKE_CURRENT_SOURCE_DIR}/lib/idl")

add_subdirectory(lib)
if (TSD_LIB)
    list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)
endif()

add_subdirectory(cppserver)
add_subdirectory(doc)
add_subdirectory(pogo)

if(TSD_DATABASE)
  add_subdirectory(scripts)
endif()

add_subdirectory(utils)

# mysql/mariadb server is found in TangoDatabase cmake logic

if(OMNIORB_PKG_FOUND)
  set(TSD_OMNIORB_VERSION ${OMNIORB_PKG_VERSION})
  set(TSD_OMNIORB_PATH ${OMNIORB_PKG_PREFIX})
else()
  # TODO Get omniORB version, see https://stackoverflow.com/a/47084079
  # $grep VERSION /usr/include/omniORB4/acconfig.h
  # define PACKAGE_VERSION "4.2.2"
  set(TSD_OMNIORB_VERSION "not-yet-supported")
  set(TSD_OMNIORB_PATH ${TANGO_OMNI_BASE})
endif()

if(ZMQ_PKG_FOUND)
  set(TSD_ZMQ_VERSION ${ZMQ_PKG_VERSION})
  set(TSD_ZMQ_PATH ${ZMQ_PKG_PREFIX})
else()
  # TODO Get ZMQ version
  set(TSD_ZMQ_VERSION "not-yet-supported")
  set(TSD_ZMQ_PATH ${TANGO_ZMQ_BASE})
endif()

# Unfortunately, TangoDatabase's FindMySQL.cmake does not cache MySQL_LIBRARY,
# only MySQL_LIBRARY_RELEASE and MySQL_LIBRARY_DEBUG.
if (NOT MySQL_LIBRARY)
  include(SelectLibraryConfigurations)
  select_library_configurations(MySQL)
endif()

set(TSD_MYSQL_CLIENT_LDLIBS ${MySQL_LIBRARY})
set(TSD_MYSQL_CLIENT_VERSION ${MySQL_VERSION})
set(TSD_MYSQL_SERVER_VERSION ${TDB_DB_VERSION})
set(TSD_MYSQL_SERVER_CONNECTION ${TDB_DB_CONNECTION_RESULT})

message(STATUS                                                                                                               "\
Summary:                                                                                                                   \n \
Configuration (TangoSourceDistribution):                                                                                   \n \
                                                                                                                           \n \
Source code location: ${CMAKE_SOURCE_DIR}                                                                                  \n \
Version: 9.4.2                                                                                                     \n \
Compiler: C++ ${CMAKE_CXX_COMPILER_ID} ${CMAKE_CXX_COMPILER_VERSION}                                                       \n \
                                                                                                                           \n \
OMNIORB PATH: ${TSD_OMNIORB_PATH}                                                                                          \n \
OMNIORB VERSION: ${TSD_OMNIORB_VERSION}                                                                                    \n \
                                                                                                                           \n \
ZMQ PATH: ${TSD_ZMQ_PATH}                                                                                                  \n \
ZMQ VERSION: ${TSD_ZMQ_VERSION}                                                                                            \n \
                                                                                                                           \n \
JAVA PATH: ${TSD_JAVA_EXEC}                                                                                                \n \
JAVA VERSION: ${TSD_JAVA_VERSION}                                                                                          \n \
                                                                                                                           \n \
Database:                                                                                                                  \n \
                                                                                                                           \n \
DB NAME: ${TANGO_DB_NAME}                                                                                                  \n \
CLIENT LIB: ${TSD_MYSQL_CLIENT_LDLIBS}                                                                                     \n \
CLIENT VERSION: ${TSD_MYSQL_CLIENT_VERSION}                                                                                \n \
SERVER VERSION: ${TSD_MYSQL_SERVER_VERSION}                                                                                \n \
CONNECTION: ${TSD_MYSQL_SERVER_CONNECTION}                                                                                 \n \
                                                                                                                           \n \
Build:                                                                                                                     \n \
                                                                                                                           \n \
libraries:              ${TSD_LIB}                                                                                         \n \
java application :      ${TSD_JAVA}                                                                                        \n \
access control server:  ${TSD_TAC}                                                                                         \n \
database server:        ${TSD_DATABASE}                                                                                    \n \
database schema create: ${TDB_DATABASE_SCHEMA}                                                                             \n \
                                                                                                                           \n \
Please check whether the configuration I detected matches what you would like to have.")
