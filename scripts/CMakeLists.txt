project(various_scripts)

if (WIN32)
    install(PROGRAMS ${CMAKE_CURRENT_SOURCE_DIR}/common.bat
            DESTINATION ${CMAKE_INSTALL_BINDIR})
else()
    set(SCRIPTS tango
                tango_wca)

    foreach(input ${SCRIPTS})
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/${input}.in ${input} @ONLY)
    install(PROGRAMS ${CMAKE_CURRENT_BINARY_DIR}/${input}
            DESTINATION ${CMAKE_INSTALL_BINDIR})
    endforeach()
endif()
