REM -------------------------------------------------------------
REM be sure TANGO_HOST is defined
REM -------------------------------------------------------------
IF NOT DEFINED TANGO_HOST (
 ECHO TANGO_HOST is not defined. Aborting!
 ECHO Please define a TANGO_HOST env. var. pointing to your TANGO database.
 ECHO TANGO_HOST syntax is tango_database_host::tango_database_port [e.g. venus::20000].
 PAUSE
 GOTO SCRIPT_END
)

set PATH=%TANGO_ROOT%\bin;%OMNI_ROOT%\bin\x86_win32;%PATH%

REM -------------------------------------------------------------
REM tango java main paths
REM -------------------------------------------------------------
set TANGO_JAVA_ROOT=%TANGO_ROOT%\share\java

REM -------------------------------------------------------------
REM log4j
REM -------------------------------------------------------------
set CLASSPATH=%CLASSPATH%;%TANGO_JAVA_ROOT%\log4j.jar

REM -------------------------------------------------------------
REM tangORB
REM -------------------------------------------------------------
set CLASSPATH=%CLASSPATH%;%TANGO_JAVA_ROOT%\JTango.jar

REM -------------------------------------------------------------
REM ATK stuffs
REM -------------------------------------------------------------
set CLASSPATH=%CLASSPATH%;%TANGO_JAVA_ROOT%\ATKCore.jar
set CLASSPATH=%CLASSPATH%;%TANGO_JAVA_ROOT%\ATKWidget.jar

REM -------------------------------------------------------------
REM  Applications
REM -------------------------------------------------------------
set CLASSPATH=%CLASSPATH%;%TANGO_JAVA_ROOT%\Jive.jar
set CLASSPATH=%CLASSPATH%;%TANGO_JAVA_ROOT%\ATKPanel.jar
set CLASSPATH=%CLASSPATH%;%TANGO_JAVA_ROOT%\ATKTuning.jar
set CLASSPATH=%CLASSPATH%;%TANGO_JAVA_ROOT%\Astor.jar
set CLASSPATH=%CLASSPATH%;%TANGO_JAVA_ROOT%\DBBench.jar
set CLASSPATH=%CLASSPATH%;%TANGO_JAVA_ROOT%\Pogo.jar
set CLASSPATH=%CLASSPATH%;%TANGO_JAVA_ROOT%\LogViewer.jar

REM -------------------------------------------------------------
REM JAVA ENV VAR
REM -------------------------------------------------------------
set JAVA_ENV_VAR=-DTANGO_HOST=%TANGO_HOST% -DTANGO_LOG_PATH=%TANGO_LOG_PATH%
