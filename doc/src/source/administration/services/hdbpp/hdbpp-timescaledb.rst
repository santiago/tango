.. _hdbpp_timescale:

:audience:`administrators, developers`, :lang:`SQL`


schema SQL source (TimescaleDb)
===============================
.. contents::
   :depth: 1

.. schema SQL source (TimescaleDb)

All the sql resources for TimescaleDb can be found on the github project https://github.com/tango-controls-hdbpp/hdbpp-timescale-project.
Under resource/schema. On top of the general architecture you can find extensions schemas for reordering, aggregates and any other features. 
